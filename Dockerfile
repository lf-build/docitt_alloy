FROM node:9 as builder

WORKDIR /src

RUN npm install -g grunt-cli 
RUN npm install -g bower 

ADD package.json /src/package.json

RUN npm install

ADD bower.json /src/bower.json
ADD .bowerrc /src/.bowerrc

RUN bower install --allow-root

ADD Gruntfile.js /src/Gruntfile.js
ADD config.js /src/config.js

ADD ./app /src/app

WORKDIR /src

RUN grunt build

RUN mkdir dist/scripts/custom
ADD app/scripts/custom/fs-snippet.js dist/scripts/custom/fs-snippet.js 

RUN rm -rf /src/app && rm -rf /src/bower_components && rm -rf /src/node_modules

FROM registry3.lendfoundry.com/docitt-alloy-core-base:1.0.0

WORKDIR /src
COPY --from=builder /src/. /src
ADD crossdomain.xml /src/dist/crossdomain.xml
ADD lua /etc/nginx/lua
ADD nginx.conf /etc/nginx/nginx.conf
ADD mapping.conf /config/mapping.conf

EXPOSE 5000

ENTRYPOINT \
    sed -i 's~CONFIG_HOST~'${ALLOY_CONFIGURATION_HOST}'~g' /etc/nginx/nginx.conf && \
    sed -i 's~SECURITY_HOST~'${ALLOY_SECURITY_HOST}'~g' /etc/nginx/nginx.conf && \
    sed -i 's~TEMPLATEMANAGER_HOST~'${ALLOY_TEMPLATEMANAGER_HOST}'~g' /etc/nginx/nginx.conf && \
    sed -i 's~ASSIGNMENTSERVICE_HOST~'${ALLOY_ASSIGNMENTSERVICE_HOST}'~g' /etc/nginx/nginx.conf && \
    sed -i 's~ASSETMANAGER_HOST~'${ALLOY_ASSETMANAGER_HOST}'~g' /etc/nginx/nginx.conf && \
    sed -i 's~USERPROFILE_HOST~'${ALLOY_USERPROFILE_HOST}'~g' /etc/nginx/nginx.conf && \
    sed -i 's~TWILIOOTP_HOST~'${ALLOY_TWILIOOTP_HOST}'~g' /etc/nginx/nginx.conf && \
    sed -i 's~APPFLOW_HOST~'${ALLOY_APPFLOW_HOST}'~g' /etc/nginx/nginx.conf && \
    sed -i 's~FILETHISDELIVERY_HOST~'${ALLOY_FILETHISDELIVERY_HOST}'~g' /etc/nginx/nginx.conf && \
    sed -i 's~THEME_HOST~'${ALLOY_THEME_HOST}'~g' /etc/nginx/nginx.conf && \
    nginx -g 'daemon off;'