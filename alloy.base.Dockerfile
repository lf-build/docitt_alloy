FROM buildpack-deps:jessie-curl

RUN apt-get update
RUN apt-get install -y nginx-extras
RUN apt-get install -y luarocks
RUN luarocks install lua-cjson
RUN luarocks install lua-resty-string