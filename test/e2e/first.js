var casper = require('casper').create({
    verbose: true,
    logLevel: "debug"
});

casper.start('localhost:9002', function() {
    // click in service loans
    var inService = '#side-menu > nav-node:nth-child(5) > li > a';
    this.click(inService);
});

casper.run();
