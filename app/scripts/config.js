Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

// browser detect
navigator.sayswho = (function () {
    var ua = navigator.userAgent,
        tem,
        M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE ' + (tem[1] || '');
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
        if (tem != null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
    return M.join(' ');
})();
var BrowserDetect = {
    init: function () {
        this.isBrowserSupport = this.isBrowserSupport();
    },
    isBrowserSupport: function () {
        window.browsersCompatible = {
            "chrome": {
                "minSupportedVersion": 23
            },
            "firefox": {
                "minSupportedVersion": 20
            },
            "safari": {
                "minSupportedVersion": 5
            },
            "ie": {
                "minSupportedVersion": 13
            },
            "edge": {
                "minSupportedVersion": 16
            }
        };
        if (window.browsersCompatible) {
            if (navigator.sayswho) {
                var browserInfoData = navigator.sayswho.split(" ");
                var browser = browserInfoData[0];
                var version = browserInfoData[1];

                if (browser) {
                    browser = browser.toLowerCase();
                    if (browser.indexOf("msie") === 0)
                        browser = "ie";
                }
                var browserConfigData = window.browsersCompatible[browser];
                if (browserConfigData) {
                    var minSupportedVersion = browserConfigData.minSupportedVersion || "";
                    if(parseFloat(minSupportedVersion)=== -1)
                        return false;
                    if (version < parseFloat(minSupportedVersion))
                        return false;
                }
            }
        }
        return true;
    }
};
BrowserDetect.init();